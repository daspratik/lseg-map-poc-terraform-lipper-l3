variable "vpc_network" {type = string}
variable "vpc_netmask" {type = string}
variable "vpc_name" {type = string}
variable "public_subnet_count" {type = number}
variable "private_subnet_count" {type = number}

variable "environment" {type = string}