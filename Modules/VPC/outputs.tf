output "vpc-id" {
  value = aws_vpc.myvpc.id
}

output "vpc-cidr" {
  value = aws_vpc.myvpc.cidr_block
}

output "public_subnet_ids" {
  value = [for x in aws_subnet.public_subnet : x.id]
}

output "private_subnet_ids" {
  value = [for x in aws_subnet.private_subnet : x.id]
}

output "public_subnet_cidr" {
  value = [for x in aws_subnet.public_subnet : x.cidr_block]
}

output "private_subnet_cidr" {
  value = [for x in aws_subnet.private_subnet : x.cidr_block]
}
