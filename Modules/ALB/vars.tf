variable "vpc-id" {type = string}
variable "alb_subnet_ids" {type = list(string)}
variable "alb_sec_groups" {type = list(string)}
variable "alb_name" {type = string}
variable "alb_tg_name" {type = string}
variable "alb_tg_ports" {type = map(any)}
variable "alb_target_type" {type = bool}
variable "alb_sg_ports" {type = map(list(string))}
variable "certificate_arn" {type = string}
variable "asg-name" {type = string}
//variable "alb_listener_ports" {type = map(any)}
//variable "alb_listener_ports" {type = list(string)}
/*
variable "alb_targets" {type = list(string)}
variable "nlb_tg_name" {
  type = string
}
variable "nlb_listener_ports" {
  type = map(any)
}
variable "nlb-sec-grp-id" {
  type = list(string)
}
variable "instance-id" {
  type = string
}
*/