output "alb_dns_endpoint" {
  description = "The FQDN of the ALB"
  value = aws_alb.my_alb.dns_name
}
output "alb_zone_id" {
  value = aws_alb.my_alb.zone_id
}
output "alb_tg_arn" {
  value = [ for target_group in aws_alb_target_group.alb-target-group : target_group.arn ]
}
/*
output "alb_id" {
  value = aws_alb.my_alb.id
}
output "alb_target_group_arns" {
  value = values(aws_alb_target_group.alb-target-group)[*].arn
}
*/