data "aws_instances" "alb_targets" {
  //instance_state_names = ["pending", "running"]
  instance_tags = {
    Terraform = "true"
  }
}
/*
locals {
  alb_listener_ports = [80,443]
}
*/
locals {
  ports_id      = keys(var.alb_tg_ports)
  //target_ids = data.aws_instances.nlb_targets.ids
  target_IDs = data.aws_instances.alb_targets.ids
  # Nested loop over both lists, and flatten the result.
  albport_targetID = (flatten([
  for port in local.ports_id : [
  for target_id in local.target_IDs : {
    port      = port
    target_id = target_id
  }
  ]
  ]))
}

locals {
  ports_ip      = keys(var.alb_tg_ports)
  //target_ids = data.aws_instances.nlb_targets.ids
  target_IPs = data.aws_instances.alb_targets.private_ips
  # Nested loop over both lists, and flatten the result.
  albport_targetIP = (flatten([
    for port in local.ports_ip : [
      for target_ip in local.target_IPs : {
        port      = port
        target_ip = target_ip
      }
    ]
  ]))
}

# Step 0: Create an Application Load Balancer
resource "aws_alb" "my_alb" {
  //depends_on                     = [data.aws_instances.alb_targets]
  //depends_on                     = [var.alb_targets]
  name                             = var.alb_name
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  internal                         = "false"
  subnets                          = var.alb_subnet_ids
  security_groups                  = var.alb_sec_groups
  enable_cross_zone_load_balancing = "true"
  enable_deletion_protection       = "false"
  enable_http2                     = "false"
  idle_timeout                     = 180
}

# Step1: Define the target group: This is going to provide a resource for use with Application Load Balancer
resource "aws_alb_target_group" "alb-target-group" {
  //depends_on           = [aws_alb.my_alb, data.aws_instances.alb_targets]
  depends_on           = [aws_alb.my_alb]
  target_type          = var.alb_target_type == true ? "instance" : "ip"
  vpc_id               = var.vpc-id
  load_balancing_algorithm_type = "least_outstanding_requests"
  deregistration_delay = 60
  //slow_start = 30
  //preserve_client_ip = "false"
  for_each             = var.alb_tg_ports
  name                 = "${var.alb_tg_name}-${each.key}"
  port                 = each.key
  protocol             = each.value
  health_check {
    enabled             = "true"
    interval            = 10
    protocol            = "HTTP"
    port                = 80
    //port              = each.key
    //protocol          = each.value
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    path                = "/index.html"
    matcher             = "200"
  }

  lifecycle {
    create_before_destroy = "true"
  }
}

# Step2: Provides an Application Load Balancer Listener resource
/*
resource "aws_lb_listener" "alb_listener" {
  depends_on        = [aws_lb_target_group.alb-target-group, data.aws_instances.alb_targets]
  load_balancer_arn = aws_lb.my_alb.arn
  for_each          = var.alb_listener_ports
  port              = each.key
  protocol          = each.value
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-target-group[each.key].arn
  }
}
*/
resource "aws_alb_listener" "http" {
  //depends_on        = [aws_alb_target_group.alb-target-group, data.aws_instances.alb_targets]
  depends_on        = [aws_alb.my_alb, aws_alb_target_group.alb-target-group]
  load_balancer_arn = aws_alb.my_alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    target_group_arn = aws_alb_target_group.alb-target-group[443].arn
    //target_group_arn = aws_lb_target_group.alb-target-group[443].id
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "https" {
  //depends_on        = [aws_alb_target_group.alb-target-group, data.aws_instances.alb_targets]
  depends_on        = [aws_alb.my_alb, aws_alb_target_group.alb-target-group]
  load_balancer_arn = aws_alb.my_alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb-target-group[80].arn
    //target_group_arn = aws_lb_target_group.alb-target-group[80].id
  }
}
/*
resource "aws_alb_target_group_attachment" "target_group" {
  depends_on       = [aws_lb.my_alb, aws_lb_target_group.alb-target-group, data.aws_instances.alb_targets]
  for_each = {
  for pair in setproduct(local.alb_listener_ports,range(length(data.aws_instances.alb_targets.ids))) : "${pair[0]} ${pair[1]}" => {
    target_group_port = pair[0]
    target_id         = pair[1]
  }
  }
  target_group_arn = aws_lb_target_group.alb-target-group[each.value.target_group_port].arn
  target_id        = data.aws_instances.alb_targets.ids[each.value.target_id]
}
*/
/*
#Step3: Provides the ability to register instances with an Application Load Balancer (ALB)
resource "aws_alb_target_group_attachment" "alb_target_group_attach" {
  depends_on       = [aws_alb.my_alb, aws_alb_target_group.alb-target-group, data.aws_instances.alb_targets]
  for_each         = var.alb_target_type == true ? { for obj in local.albport_targetID : "${obj.target_id}.${obj.port}" => obj } : { for obj in local.albport_targetIP : "${obj.target_ip}.${obj.port}" => obj } # We need a map to use for_each, so we convert our list into a map by adding a unique key:
  //target_group_arn = aws_lb_target_group.alb-target-group[each.value.port].arn
  target_group_arn = aws_alb_target_group.alb-target-group[each.value.port].id
  port             = each.value.port
  target_id        = var.alb_target_type == true ? each.value.target_id : each.value.target_ip
}

#Step3: Provides the ability to register ASG instances with an Application Load Balancer (ALB)
resource "aws_autoscaling_attachment" "asg_alb_attach" {
  //depends_on = [aws_alb.my_alb, aws_alb_target_group.alb-target-group, data.aws_instances.alb_targets]
  //for_each         = var.alb_target_type == true ? { for obj in local.albport_targetID : "${obj.target_id}.${obj.port}" => obj } : { for obj in local.albport_targetIP : "${obj.target_ip}.${obj.port}" => obj } # We need a map to use for_each, so we convert our list into a map by adding a unique key:
  depends_on = [aws_alb.my_alb, aws_alb_target_group.alb-target-group]
  for_each             = var.alb_tg_ports
  autoscaling_group_name = var.asg-name
  lb_target_group_arn = aws_alb_target_group.alb-target-group[each.key].arn
}
*/
/*
#Step4: Security group used by ALB
resource "aws_security_group" "alb-sg" {
  name   = "alb_sg"
  vpc_id = var.vpc-id
  dynamic "ingress" {
    for_each = var.alb_sg_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "my-alb-sg" {
  name   = "my-alb-sg"
  vpc_id = var.vpc-id
}
resource "aws_security_group_rule" "inbound_ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.my-alb-sg.id}"
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "inbound_http" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.my-alb-sg.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.my-alb-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
*/
