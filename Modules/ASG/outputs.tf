output "autoscaling-group-name" {
  value = aws_autoscaling_group.L3-Dev-ASG.id
}
output "ASG-instance-count" {
  value = aws_autoscaling_group.L3-Dev-ASG.desired_capacity
}