# Data Block
data "aws_availability_zones" "asg_az" {
  state = "available"
}

# Create IAM role, and attach ec2-profile and AmazonSSMManagedInstanceCore policy to the role
resource "aws_iam_role" "l3-dev-iam-role" {
  name        = "l3-dev-ssm-role"
  description = "The SSM role for the L3 Dev instances"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": {
"Effect": "Allow",
"Principal": {"Service": "ec2.amazonaws.com"},
"Action": "sts:AssumeRole"
  }
}
EOF
  tags = {
    stack = "L3-Lipper-Dev"
  }
}
resource "aws_iam_role_policy_attachment" "l3-dev-ssm-policy" {
  role       = aws_iam_role.l3-dev-iam-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
resource "aws_iam_instance_profile" "l3-dev-iam-profile" {
  name = "ec2_profile"
  role = aws_iam_role.l3-dev-iam-role.name
}
/*
# Create AWS Launch configuration
resource "aws_launch_configuration" "L3-Dev-LC" {
  name = "L3-Dev-LC"
  //name_prefix = "a250240-"
  image_id = var.image-id
  instance_type = var.instance-type
  key_name = var.PUBLIC_KEY
  security_groups = var.instance-sec-grp-ids
  associate_public_ip_address = "true"
  enable_monitoring = "true"
  user_data = file(var.USER_DATA)
  iam_instance_profile = aws_iam_instance_profile.l3-dev-iam-profile.name
  ebs_optimized = "false"
  root_block_device {
    delete_on_termination = "true"
    encrypted = "true"
    iops = 3000
    throughput = 125
    volume_size = var.root_vol_size
    volume_type = var.vol_type
  }
  ebs_block_device {
    no_device = "false"
    device_name = "/dev/xvdh"
    delete_on_termination = "true"
    encrypted = "true"
    iops = 3000
    throughput = 125
    volume_size = var.data_vol_size
    volume_type = var.vol_type
  }
  ebs_block_device {
    no_device = "false"
    device_name = "/dev/xvdj"
    delete_on_termination = "true"
    encrypted = "true"
    iops = 3000
    throughput = 125
    volume_size = var.data_vol_size
    volume_type = var.vol_type
  }
  lifecycle {
    create_before_destroy = "true"
  }
}
*/
# Create AWS Launch Template
resource "aws_launch_template" "L3-Dev-LT" {
  name = "L3-Dev-LT"
  //name_prefix = "a250240-"
  image_id = var.image-id
  instance_type = var.instance-type
  ebs_optimized = "false"
  key_name = var.PUBLIC_KEY
  //user_data = file(var.USER_DATA)
  user_data = filebase64(var.USER_DATA)
  update_default_version = "true"
  disable_api_termination = "false"
  #vpc_security_group_ids = var.instance-sec-grp-ids
  iam_instance_profile {
    name = aws_iam_instance_profile.l3-dev-iam-profile.name
  }
  network_interfaces {
    delete_on_termination = "true"
    associate_public_ip_address = "true"
    security_groups = [var.instance-sec-grp-ids]
  }
  block_device_mappings {
  # Root volume
    device_name = "/dev/sda1"
    no_device = "false"
    virtual_name = "root"
      ebs {
        //kms_key_id = ""
        delete_on_termination = "true"
        encrypted             = "true"
        volume_size           = var.root_vol_size
        volume_type           = var.vol_type
        iops                  = var.iops
        throughput            = var.throughput
    }
  }
  block_device_mappings {
    # Data volume
    device_name = "/dev/xvdh"
    no_device = "false"
    virtual_name = "Data Volume 1"
    ebs {
      //kms_key_id = ""
      encrypted             = "true"
      delete_on_termination = "true"
      volume_size           = var.data_vol_size
      volume_type           = var.vol_type
      iops                  = var.iops
      throughput            = var.throughput
    }
  }
  block_device_mappings {
    # Data volume
    device_name = "/dev/xvdj"
    no_device = "false"
    virtual_name = "Data Volume 2"
    ebs {
      //kms_key_id = ""
      encrypted = "true"
      delete_on_termination = "true"
      volume_size = var.data_vol_size
      volume_type = var.vol_type
      iops = var.iops
      throughput = var.throughput
    }
  }
  lifecycle {
    create_before_destroy = "true"
  }
}

# Create AWS Auto Scaling Group
resource "aws_autoscaling_group" "L3-Dev-ASG" {
  //depends_on         = [aws_launch_configuration.L3-Dev-LC]
  //name               = "${aws_launch_configuration.L3-Dev-LC.name}-ASG"
  //availability_zones = data.aws_availability_zones.asg_az.names
  //load_balancers     = [var.alb-id]
  //launch_configuration = aws_launch_configuration.L3-Dev-LC.name
  depends_on           = [aws_launch_template.L3-Dev-LT]
  name                 = "${aws_launch_template.L3-Dev-LT.name}-ASG"
  min_size             = var.min-size
  desired_capacity     = var.desired-size
  max_size             = var.max-size
  vpc_zone_identifier  = var.asg-subnets
  force_delete         = "true"
  health_check_type    = var.asg-health-check-type
  health_check_grace_period = 300
  target_group_arns    = var.alb-target-group-arns
  launch_template {
    id      = aws_launch_template.L3-Dev-LT.id
    version = aws_launch_template.L3-Dev-LT.latest_version #"$Latest"
  }
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"
  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = "true"
  }
  tag {
    key                 = "Name"
    value               = "Lipper-L3-Dev"
    propagate_at_launch = "true"
  }
  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = "true"
  }
}
# Create AWS Auto Scaling Policy
resource "aws_autoscaling_policy" "L3-Dev-scaleup" {
  name = "L3-Dev-scaleup-policy"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = aws_autoscaling_group.L3-Dev-ASG.name
  policy_type = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "L3-Dev-alarm-up" {
  alarm_name = "L3-Dev-alarm-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "70"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.L3-Dev-ASG.name
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [aws_autoscaling_policy.L3-Dev-scaleup.arn]
}
resource "aws_autoscaling_policy" "L3-Dev-scaledown" {
  name = "L3-Dev-scaledown-policy"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = aws_autoscaling_group.L3-Dev-ASG.name
  policy_type = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "L3-Dev-alarm-down" {
  alarm_name = "L3-Dev-alarm-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "30"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.L3-Dev-ASG.name
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [aws_autoscaling_policy.L3-Dev-scaledown.arn]
}