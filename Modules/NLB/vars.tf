variable "vpc-id" {type = string}
variable "nlb_subnet_ids" {type = list(string)}
variable "hdb_nlb_name" {type = string}
variable "hdb_nlb_tg_name" {type = string}
variable "hdb_nlb_tg_ports" {type = map(any)}
variable "hdb_nlb_listener_ports" {type = map(any)}
variable "nlb_target_type" {type = bool}
/*
variable "nlb_tg_name" {
  type = string
}
variable "nlb_listener_ports" {
  type = map(any)
}
variable "nlb-sec-grp-id" {
  type = list(string)
}
variable "instance-id" {
  type = string
}
*/