output "nlb_dns_endpoint" {
  description = "The FQDN of the NLB"
  value = aws_lb.my_nlb_A.dns_name
}

output "nlb_target_ids" {
  value = data.aws_instances.nlb_targets.ids
}