data "aws_instances" "nlb_targets" {
  //instance_state_names = ["running"]
  instance_tags = {
	Terraform = "true"
  }
}

locals {
  ports_id      = keys(var.hdb_nlb_tg_ports)
  //target_ids = data.aws_instances.nlb_targets.ids
  target_IDs = data.aws_instances.nlb_targets.ids
  # Nested loop over both lists, and flatten the result.
  nlbport_targetID = (flatten([
  for port in local.ports_id : [
  for target_id in local.target_IDs : {
    port      = port
    target_id = target_id
  }
  ]
  ]))
}

locals {
  ports_ip      = keys(var.hdb_nlb_tg_ports)
  //target_ids = data.aws_instances.nlb_targets.ids
  target_IPs = data.aws_instances.nlb_targets.private_ips
  # Nested loop over both lists, and flatten the result.
  nlbport_targetIP = (flatten([
    for port in local.ports_ip : [
      for target_ip in local.target_IPs : {
        port      = port
        target_ip = target_ip
      }
    ]
  ]))
}

# Create Network Load Balancer
resource "aws_lb" "my_nlb_A" {
  depends_on = [data.aws_instances.nlb_targets]
  name                             = var.hdb_nlb_name
  ip_address_type                  = "ipv4"
  load_balancer_type               = "network"
  internal                         = "false"
  subnets                          = var.nlb_subnet_ids
  enable_cross_zone_load_balancing = "true"
  enable_deletion_protection       = "false"
}

resource "aws_lb_target_group" "nlb_target_group_A" {
  depends_on           = [aws_lb.my_nlb_A]
  //target_type        = "instance"
  //target_type        = "ip"
  target_type          = var.nlb_target_type == true ? "instance" : "ip"
  vpc_id               = var.vpc-id
  deregistration_delay = 300
  preserve_client_ip   = "false"
  for_each             = var.hdb_nlb_tg_ports
  name                 = "${var.hdb_nlb_tg_name}-${each.key}"
  port                 = each.key
  protocol             = each.value

  health_check {
    enabled             = "true"
    interval            = 30
    port                = "traffic-port"
    protocol            = "TCP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    //timeout = 10
    //path      = "/index.html"
    //matcher = "200"
  }

  lifecycle {
    create_before_destroy = "true"
  }

}

resource "aws_lb_listener" "nlb_listener_A" {
  depends_on        = [aws_lb_target_group.nlb_target_group_A]
  load_balancer_arn = aws_lb.my_nlb_A.arn
  for_each          = var.hdb_nlb_listener_ports
  port              = each.key
  protocol          = each.value
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_target_group_A[each.key].arn
  }
}

resource "aws_lb_target_group_attachment" "target_group_attach_A" {
  depends_on       = [aws_lb.my_nlb_A, aws_lb_target_group.nlb_target_group_A]
  for_each         = var.nlb_target_type == true ? { for obj in local.nlbport_targetID : "${obj.target_id}.${obj.port}" => obj } : { for obj in local.nlbport_targetIP : "${obj.target_ip}.${obj.port}" => obj } # We need a map to use for_each, so we convert our list into a map by adding a unique key:
  target_group_arn = aws_lb_target_group.nlb_target_group_A[each.value.port].arn
  port             = each.value.port
  target_id        = var.nlb_target_type == true ? each.value.target_id : each.value.target_ip
  //availability_zone = "all"
}

