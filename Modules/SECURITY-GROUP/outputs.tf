output "instance-sg-id" {
  value = aws_security_group.instance_sg.id
}
/*
output "bastion-sg-id" {
  value = aws_security_group.bastion_sg.id
}
*/
output "alb-sg-id" {
  value = aws_security_group.alb_sg.id
}
