resource "aws_security_group" "instance_sg" {
  name    = "instance_sg"
  vpc_id  = var.vpc-id

  dynamic "ingress" {
    for_each = var.instance_sg_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
/*
resource "aws_security_group" "bastion_sg" {
  name    = "bastion_sg"
  vpc_id  = var.vpc-id

  dynamic "ingress" {
    for_each = var.bastion_sg_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
*/
resource "aws_security_group" "alb_sg" {
  name   = "alb_sg"
  vpc_id = var.vpc-id
  dynamic "ingress" {
    for_each = var.alb_sg_ports
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

/*
resource "aws_security_group_rule" "rule_public_sg_ingress" {
  type = "ingress"
  for_each = var.public_sg_ports
      from_port = ingress.key
      to_port = ingress.key
      source_security_group_id = [var.nlb_sg_id]
      protocol = "tcp"
  security_group_id = aws_security_group.public_sg.id
}
*/
