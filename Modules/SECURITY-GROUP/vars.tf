variable "vpc-id" {type = string}
variable "instance_sg_ports" {type = map(list(string))}
//variable "bastion_sg_ports" {type = map(list(string))}
variable "alb_sg_ports" {type = map(list(string))}
/*
variable "nlb_sg_id" {
  type = string
}
variable "nlb_sg_ports" {
  type = map(list(string))
}
*/