variable "root_domain_name" {type = string}
variable "domain_name" {type = string}
variable "subdomain_name" {type = string}
variable "alias_name" {type = string}
variable "alias_zone_id" {type = string}