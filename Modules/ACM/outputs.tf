output "subdomain-name" {
  value = var.subdomain_name
}

output "acm-certificate-arn" {
  value = aws_acm_certificate.acm_certificate.arn
}