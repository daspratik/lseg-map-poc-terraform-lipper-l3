# Create KMS key
resource "aws_kms_key" "ic4_kms_key" {
  //count  = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  description = "KMS key for EBS Volume encryption of SAP instances"
  customer_master_key_spec = var.key_spec
  is_enabled = var.kms_enabled
  enable_key_rotation = var.kms_rotation
  deletion_window_in_days = var.kms_deletion_window
  tags = {
    Name = "${var.name}${count.index+1}"
  }
  policy = <<EOF
  {
   "Id": "key-consolepolicy-3",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${var.user_arn}"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${var.role_arn}"
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                   "AWS": "${var.role_arn}"
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKeyWithoutPlaintext",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow attachment of persistent resources",
            "Effect": "Allow",
            "Principal": {
                  "AWS": "${var.role_arn}"
            },
            "Action": [
                "kms:CreateGrant",
                "kms:ListGrants",
                "kms:RevokeGrant"
            ],
            "Resource": "*",
            "Condition": {
                "Bool": {
                    "kms:GrantIsForAWSResource": "true"
                }
            }
        }
    ]
}
  EOF
}

resource "aws_kms_alias" "ic4_kms_alias" {
  //count  = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  target_key_id = aws_kms_key.ic4_kms_key.*.key_id[count.index]
  name          = "alias/${var.name}${count.index+1}"
}