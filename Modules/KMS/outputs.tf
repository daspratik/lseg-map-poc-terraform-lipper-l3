output "kms_key_id" {
  value = [for x in aws_kms_key.ic4_kms_key : x.id]
}

output "kms_key_arn" {
  value = [for x in aws_kms_key.ic4_kms_key : x.arn]
}