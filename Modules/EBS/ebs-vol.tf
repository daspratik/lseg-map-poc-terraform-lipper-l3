# Data Block
data "aws_availability_zones" "az" {
  state = "available"
}

# Create EBS volumes
resource "aws_ebs_volume" "ebs-vol01" {
  //count             = length(var.public_subnet_id)
  count = var.instance-count
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol01-size
  type              = var.vol-type
  //kms_key_id      =  var.kms-key-ids[count.index]
  encrypted         =   "true"
}

resource "aws_ebs_volume" "ebs-vol02" {
  //count             = length(var.public_subnet_id)
  count = var.instance-count
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol02-size
  type              = var.vol-type
  //kms_key_id      =  var.kms-key-ids[count.index]
  encrypted         =   "true"
}

# Attach EBS Volumes to EC2 instances
resource "aws_volume_attachment" "ebs-vol01-attachment" {
  depends_on  = [aws_ebs_volume.ebs-vol01]
  //count       = length(var.public_subnet_id)
  count = var.instance-count
  device_name = "/dev/xvdh"
  instance_id = var.instance-ids[count.index]
  volume_id   = aws_ebs_volume.ebs-vol01.*.id[count.index]
  force_detach      = "true"
  skip_destroy      = "false"
}

resource "aws_volume_attachment" "ebs-vol02-attachment" {
  depends_on = [aws_ebs_volume.ebs-vol02]
  //count       = length(var.public_subnet_id)
  count = var.instance-count
  device_name = "/dev/xvdj"
  instance_id = var.instance-ids[count.index]
  volume_id   = aws_ebs_volume.ebs-vol02.*.id[count.index]
  force_detach      = "true"
  skip_destroy      = "false"
}