# Data Block
data "aws_availability_zones" "instance_az" {
  state = "available"
}
data "aws_vpc" "instance_vpc" {
  id = var.vpc_id
}
data "aws_subnet_ids" "instance_subnets" {
  vpc_id = data.aws_vpc.instance_vpc.id
}
/*
data "aws_caller_identity" "current" {}
data "aws_ami" "Lipper-QDB" {
  most_recent = true
  filter {
    name   = "Application"
    values = ["Lipper-QDB"]
  }
  owners = [data.aws_caller_identity.current.account_id] # your account id
}

data "aws_ami" "amzlinux" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
resource "aws_iam_instance_profile" "ec2_profile" {
  name  = "terraform-role"
  role  = "terraform-role"
}
*/
# Create IAM role, and attach ec2-profile and AmazonSSMManagedInstanceCore policy to the role
resource "aws_iam_role" "l3-dev-iam-role" {
  name        = "l3-dev-ssm-role"
  description = "The role for the L3 developer instances"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": {
"Effect": "Allow",
"Principal": {"Service": "ec2.amazonaws.com"},
"Action": "sts:AssumeRole"
}
}
EOF
  tags = {
    stack = "L3-Lipper-Dev"
  }
}
resource "aws_iam_role_policy_attachment" "l3-dev-ssm-policy" {
  role       = aws_iam_role.l3-dev-iam-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
resource "aws_iam_instance_profile" "l3-dev-iam-profile" {
  name = "ec2_profile"
  role = aws_iam_role.l3-dev-iam-role.name
}
# Create ec2 server instance
resource "aws_instance" "ec2_instance" {
  //count                     = var.is_cluster == true ? 2 : 1
  //ami                       = data.aws_ami.Lipper-QDB.id
  //subnet_id                 = lookup(var.instance_subnet_id, count.index)
  //key_name                  = var.PUBLIC_KEY[count.index]
  //subnet_id                 = element(tolist(data.aws_subnet_ids.instance_subnets.ids), count.index)
  iam_instance_profile        = aws_iam_instance_profile.l3-dev-iam-profile.name
  subnet_id                   = var.instance_subnet_ids[count.index]
  count                       = var.is_cluster == true ? var.instance_count : 1
  ami                         = var.packer-ami
  instance_type               = var.instance-type
  key_name                    = var.PUBLIC_KEY
  user_data                   = file(var.USER_DATA)
  disable_api_termination     = "false"
  associate_public_ip_address = "true"
  vpc_security_group_ids      = var.instance-sec-grp-ids
  tenancy                     = var.instance-tenancy
  source_dest_check           = "true"
  availability_zone           = data.aws_availability_zones.instance_az.names[count.index]
  lifecycle {
    ignore_changes = [ami]
  }

  tags = {
    Name         = "${var.name}${count.index + 1}"
    Application   = var.application
    "Business Owner" = var.business-owner
    "Business Area" = var.business-area
    Owner = var.owner
    Environment = var.environment
    Service     = var.service
    Platform = var.platform
    "OS Version" = var.os-version
	Terraform = "true"
  }

  # root disk
  root_block_device {
    volume_size           = var.root_vol_size
    volume_type           = var.root_vol_type
    delete_on_termination = "true"
    encrypted             = "true"
    //kms_key_id            = var.kms-key-ids[count.index]
  }
}

/*
# Create EBS volumes
resource "aws_ebs_volume" "ebs-vol01" {
  //count             = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol01-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-ids[count.index]
}

resource "aws_ebs_volume" "ebs-vol02" {
  //count             = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol02-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-ids[count.index]
}

resource "aws_ebs_volume" "ebs-vol03" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol03-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol04" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol04-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol05" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol05-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol06" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol06-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol07" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol07-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol08" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol08-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol09" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol09-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

resource "aws_ebs_volume" "ebs-vol10" {
  count             = var.is_cluster == true ? 2 : 1
  availability_zone = data.aws_availability_zones.az.names[count.index]
  size              = var.vol10-size
  type              = var.vol-type
  encrypted           = "true"
  kms_key_id          = var.kms-key-id
}

# Attach EBS Volumes to EC2 instances we created earlier
resource "aws_volume_attachment" "ebs-vol01-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol01]
  //count        = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  device_name  = "/dev/xvdh"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol01.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol02-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol02]
  //count        = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  device_name  = "/dev/xvdj"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol02.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol03-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol03]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdd"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol03.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol04-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol04]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sde"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol04.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol05-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol05]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdf"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol05.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol06-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol06]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdg"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol06.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol07-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol07]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdh"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol07.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol08-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol08]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdi"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol08.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol09-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol09]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sds"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol09.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol10-attachment" {
  depends_on   = [aws_ebs_volume.ebs-vol10]
  count        = var.is_cluster == true ? 2 : 1
  device_name  = "/dev/sdz"
  instance_id  = aws_instance.ec2_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol10.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}
*/
resource "null_resource" "remote-script-execute" {
  depends_on = [aws_instance.ec2_instance]
  //depends_on = [aws_instance.ec2_instance, aws_ebs_volume.ebs-vol01, aws_ebs_volume.ebs-vol02]
  //depends_on = [aws_instance.ec2_instance, aws_ebs_volume.ebs-vol01, aws_ebs_volume.ebs-vol02, aws_ebs_volume.ebs-vol03, aws_ebs_volume.ebs-vol04, aws_ebs_volume.ebs-vol05, aws_ebs_volume.ebs-vol06, aws_ebs_volume.ebs-vol07, aws_ebs_volume.ebs-vol08, aws_ebs_volume.ebs-vol09, aws_ebs_volume.ebs-vol10]
  //count      = var.is_cluster == true ? 2 : 1
  count = var.is_cluster == true ? var.instance_count : 1
  connection {
    type        = "ssh"
    host        = aws_instance.ec2_instance[count.index].public_ip
    user        = var.USERNAME
    //private_key = file(var.PRIVATE_KEY[count.index])
    private_key = file(var.PRIVATE_KEY)
  }
  provisioner "file" {
    source      = "script.sh"
    destination = "/home/ec2-user/script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /home/ec2-user/script.sh",
      "sudo /home/ec2-user/script.sh",
    ]
  }
  /*
  provisioner "local-exec" {
    command = "chmod 400 ${var.PRIVATE_KEY}"
  }
  */
 }