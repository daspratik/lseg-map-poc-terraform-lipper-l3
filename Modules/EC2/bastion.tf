data "aws_availability_zones" "bastion_az" {
  state = "available"
}
data "aws_vpc" "bastion_vpc" {
  id = var.vpc_id
}
data "aws_subnet_ids" "bastion_subnets" {
  vpc_id = data.aws_vpc.bastion_vpc.id
}
# Get latest Amazon Linux 2 AMI
data "aws_ami" "amzlinux" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
# Get latest Windows Server 2019 AMI
data "aws_ami" "windows-2019" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base*"]
  }
}

resource "random_id" "index" {
  byte_length = 2
}

locals {
  subnet_ids_list = tolist(data.aws_subnet_ids.bastion_subnets.ids)
  subnet_ids_random_index = random_id.index.dec % length(data.aws_subnet_ids.bastion_subnets.ids)
  bastion_subnet_id = local.subnet_ids_list[local.subnet_ids_random_index]
}
/*
resource "random_integer" "random_int" {
  max = 2
  min = 0
  keepers = {
    random_num = var.random_number
  }
}
*/
resource "aws_instance" "bastion" {
  ami                           = var.is_bastion_windows == true ? data.aws_ami.windows-2019.id : data.aws_ami.amzlinux.id
  instance_type                 = var.bastion_instance_type
  count                         = var.is_bastion == true ? 1 : 0
  key_name                      = var.PUBLIC_KEY
  //subnet_id                   = var.instance_subnet_ids[random_integer.random_int.seed]
  subnet_id                     = local.bastion_subnet_id
  vpc_security_group_ids        = var.bastion-sec-grp-ids
  disable_api_termination       = "false"
  associate_public_ip_address   = "true"
  tenancy                       = var.instance-tenancy
  source_dest_check             = "true"
  //availability_zone           = data.aws_availability_zones.az_bastion.names[random_integer.random_int.seed]
  lifecycle {
    ignore_changes = [subnet_id]
  }
  tags = {
    Name = "Bastion-Host"
  }
  /*
  # root disk
  root_block_device {
    volume_size           = var.root_vol_size
    volume_type           = var.root_vol_type
    delete_on_termination = "true"
    encrypted             = "true"
    //kms_key_id          = var.kms-key-ids[count.index]
  }
*/
}