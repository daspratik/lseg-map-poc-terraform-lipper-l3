output "instance-id" {
  description = "ID of EC2 instance(s)"
  value       = [for x in aws_instance.ec2_instance : x.id]
}

output "bastion_public_ip" {
  value = [for x in aws_instance.bastion : x.public_ip]
}

output "public_ip" {
  value = [for x in aws_instance.ec2_instance : x.public_ip]
}

output "private_ip" {
  value = [for x in aws_instance.ec2_instance : x.private_ip]
}
/*
output "instance_id" {
  value = aws_instance.ec2_instance.*.id
}
*/


