output "ssh-key-private" {
  description = "ssh pem key contents"
  value       = tls_private_key.ssh_private_key.*.private_key_pem
}

output "ssh-key-public" {
  description = "ssh key pair name"
  value       = aws_key_pair.ssh_key_pair.*.key_name
}