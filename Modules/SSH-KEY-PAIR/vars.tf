variable "key-pair-count" { type = number }
variable "key-pair-name" { type = string }
variable "rsa_bits" { type = number }