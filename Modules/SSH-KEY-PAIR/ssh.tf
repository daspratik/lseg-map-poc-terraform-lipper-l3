# Generate a private TLS key using RSA encryption with a length of 4096 bits
resource "tls_private_key" "ssh_private_key" {
  count     = var.key-pair-count
  algorithm = "RSA"
  rsa_bits  = var.rsa_bits
}

# Leverage the TLS private key to generate a SSH public/private key pair
resource "aws_key_pair" "ssh_key_pair" {
  depends_on = [tls_private_key.ssh_private_key]
  count      = var.key-pair-count
  key_name   = "${var.key-pair-name}${count.index + 1}"
  public_key = tls_private_key.ssh_private_key[count.index].public_key_openssh

  # when creating, save the private key to the local Terraform directory
  provisioner "local-exec" {
    when = create
    command = "echo '${tls_private_key.ssh_private_key[count.index].private_key_pem}' > /home/ec2-user/AWS_Cloud_Infra_Terraform_v2.0.5-Modified/Server-Instance/${var.key-pair-name}${count.index + 1}.pem"
  }

  # When this resources is destroyed, delete the associated key from the file system
  provisioner "local-exec" {
    when    = destroy
    command = "rm -rf ${self.id}.pem"
  }
}

# Apply correct file/directory level permission
resource "local_file" "pem-file" {
  depends_on            = [tls_private_key.ssh_private_key]
  count                 = var.key-pair-count
  content               = tls_private_key.ssh_private_key[count.index].private_key_pem
  filename              = "${var.key-pair-name}${count.index + 1}.pem"
  directory_permission  = "0777"
  file_permission       = "0400"
}

