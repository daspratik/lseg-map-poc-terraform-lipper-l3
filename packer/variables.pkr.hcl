#####################
# General Variables #
#####################
variable "ami_name" {
  description = "name of the custom AMI"
  default     = "Lipper-L3-Dev"
}

variable "instance_type" {
  description = "instance type for packer build"
  default     = "t2.micro"
}

#####################
# AWS VPC Variables #
#####################
variable "aws_region" {
  description = "AWS Region for AMI creation"
  default = "us-west-2"
}

variable "vpc_id" {
  description = "vpc ID for AMI creation"
  default = "vpc-096b9b891738a5de1"
}

variable "subnet_id" {
  description  = "subnet ID for AMI creation"
  default = "subnet-09a3a704bf6d2b53c"
}

#####################
# Connection Variables #
#####################
/*
variable "role_arn" {
  description  = "Role ARN required by Packer service to create the AMI"
  default      = "arn:aws:iam::649711108397:role/terraform-server-role"
}

variable "session_name" {
  description  = "STS session required by Packer service to create the AMI"
  default      = "packer-session"
}
*/
variable "communicator" {
  description  = "communication method used for instance"
  default      = "ssh"
}

variable "ssh_username" {
  description = "ssh username for packer to use for provisioning"
  default     = "ec2-user"
}
