#!/bin/bash
sleep 60
sudo yum update -y
sudo yum install wget -y
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform
sudo yum install -y httpd
sudo yum install unzip -y
sudo yum install -y awslogs
sudo yum install -y git
sudo amazon-linux-extras install java-openjdk11
cd /tmp
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
