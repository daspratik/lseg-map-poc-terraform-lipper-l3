packer {
  required_plugins {
    amazon = {
      version = ">=1.1.4"
      source = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "Lipper-QDB-ami"{
  # AWS AMI data source lookup
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm-*-x86_64-ebs"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  # AWS EC2 parameters
  ami_name      = "${var.ami_name}-${regex_replace(timestamp(), "[- TZ:]", "")}"
  instance_type = var.instance_type
  region        = var.aws_region
  subnet_id     = var.subnet_id
  vpc_id        = var.vpc_id
/*
  assume_role {
    role_arn      = var.role_arn
    session_name  = var.session_name
    //external_id = "EXTERNAL_ID"
  }
*/
  # provisioning connection parameters
  communicator                 = var.communicator
  ssh_username                 = var.ssh_username
  //ssh_interface              = "session_manager"
  iam_instance_profile         = "MyCloud-Role"

  tags = {
    Environment     = "Dev"
    Name            = "Lipper-L3-Dev-${regex_replace(timestamp(), "[- TZ:]", "")}"
    PackerBuilt     = "true"
    PackerTimestamp = regex_replace(timestamp(), "[- TZ:]", "")
    Application     = "Lipper-L3"
  }
}

build {
  sources = ["source.amazon-ebs.Lipper-QDB-ami"]
  provisioner "shell" {
    scripts = [
      "./bootscript.sh",
    ]
  }
}