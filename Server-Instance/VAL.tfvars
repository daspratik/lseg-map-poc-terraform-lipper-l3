##################
# General Values #
#################
//aws_profile = "sap-val"
aws_region     = "us-west-2"
role_arn       = "arn:aws:iam::163898482461:role/MyCloud-Role"
//role_arn     = "arn:aws:iam::163898482461:role/terraform-role" # Role ARN for the Target Account
//role_arn     = "arn:aws:iam::649711108397:role/IaC-Role" # Role ARN for the Account where resources will be provisioned
session_name   = "tf_sts_session"
###############
# VPC Values #
##############
vpc_name             = "Lipper-L3-Dev"
vpc_network          = "10.60.192.0"
vpc_netmask          = "18"
public_subnet_count  = 3
private_subnet_count = 0
environment          = "Dev"
##########################
# Security Group Values #
#########################
/*
bastion_sg_ports = {
  "22"    = ["0.0.0.0/0"]
  "3389"  = ["0.0.0.0/0"]
}
*/
instance_sg_ports = {
  "22"  = ["0.0.0.0/0"]
  "80"  = ["0.0.0.0/0"]
  "443" = ["0.0.0.0/0"]
}
alb_sg_ports = {
  "80"  = ["0.0.0.0/0"]
  "443" = ["0.0.0.0/0"]
}
/*
###############
# KMS Values #
##############
user_arn            = "arn:aws:iam::163898482461:root" # Root user ARN for the target Account
key_spec            = "SYMMETRIC_DEFAULT"
kms_rotation        = "true"
kms_enabled         = "true"
kms_deletion_window = 7
###############
# EC2 Values #
##############
name                  = "Lipper-L3-Instance"
packer_ami            = "ami-031a6fa4f9e37d0d3"
environment           = "Dev"
application           = "Lipper-L3"
business-area         = "LSEG"
business-owner        = "Paul Ashton"
owner                 = "Don Teetz"
service               = "Lipper"
platform              = "Linux"
os-version            = "Amazon Linux 2"
instance-tenancy      = "default"
instance-type         = "t2.micro"
is_cluster            = "true"
is_bastion            = "true"
is_bastion_windows    = "false"
bastion-instance-type = "t2.micro"
*/
/*
instance_subnet_id = {
  0 = "subnet-0b9bd1c91406e272a"
  1 = "subnet-0034aae59473fdbb8"
}
vpc-sec-grp-ids = ["sg-0aaeb397f2f2b1ae8", "sg-0ac2aff4ae4c9e0f5"]
*/
########################
# RSA Key Pair Values #
#######################
rsa-bits      = 4096
key-pair-name = "Lipper-L3-Dev"
###############
# ASG Values #
##############
//image-id = "ami-001bf060163b2c3b4"
USER_DATA = "USER_DATA.sh"
vol-type = "gp3"
root-vol-size = 10
data-vol-size = 20
iops = 3000
throughput = 125
asg-health-check-type = "ELB"
instance-type = "t2.micro"
min-size = 2
desired-size = 4
max-size = 6
//USERNAME      = "ec2-user"
//vol01-size    = 8
//vol02-size    = 6
//vpc-id = "vpc-0153f74b489964574"
//nlb_subnet_ids  = ["subnet-0b9bd1c91406e272a", "subnet-0034aae59473fdbb8"]
###############
# NLB Values #
##############
/*
nlb_target_type = "true"
hdb_nlb_name    = "VAL-NLB"
hdb_nlb_tg_name = "VAL-TG"
hdb_nlb_listener_ports = {
  80  = "TCP"
  443 = "TCP"
}
hdb_nlb_tg_ports = {
  80  = "TCP"
  443 = "TCP"
}
*/
###############
# ALB Values #
##############
alb_target_type = "true"
alb_name    = "MY-ALB"
alb_tg_name = "MY-ALB-TG"
alb_tg_ports = {
  80  = "HTTP"
  443 = "HTTPS"
}
//alb_listener_ports = [80,443]
/*
alb_listener_ports = {
  80  = "HTTP"
  443 = "HTTPS"
}
*/
###############
# ACM Values #
##############
domain-name           = "*.learning-aws-cloud-by-hands-on.click"
root-domain-name      = "learning-aws-cloud-by-hands-on.click"
subdomain-name        = "www.learning-aws-cloud-by-hands-on.click"

/*
//ami_id = data.aws_ami.suse-linux-12-sp3.id
//ami_id = "ami-00ee4df451840fa9d"
//kms-key-id = "arn:aws:kms:eu-west-1:447244267531:key/1f959a97-fb57-42e6-825f-33615f156f80"
//name = "EU2V-SAPVHV"
vol03-size    = 225
vol04-size    = 512
vol05-size    = 2048
vol06-size    = 6
vol07-size    = 300
vol08-size    = 300
vol09-size    = 50
vol10-size    = 50
*/

