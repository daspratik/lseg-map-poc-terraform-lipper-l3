output "vpc_cidr" {
  value = module.L3-Dev-VPC.vpc-cidr
}
output "public_subnet_cidr" {
  value = [for x in module.L3-Dev-VPC.public_subnet_cidr : x]
}
output "ALB_dns_endpoint_URL" {
  description = "URL of load balancer"
  value = module.L3-Dev-ALB.alb_dns_endpoint
}
output "web_server_count" {
  description = "Number of instances provisioned"
  value       = module.L3-Dev-ASG.ASG-instance-count
}
output "ACM-Certificate-arn" {
  value = module.L3-Dev-ACM.acm-certificate-arn
}
output "subdomain-url" {
  value = module.L3-Dev-ACM.subdomain-name
}
/*
output "bastion_public_ip" {
  value = module.Instance-Val.bastion_public_ip
}
output "instance_node_public_ips" {
  value = [for x in module.Instance-Val.public_ip : x]
}
output "instance_node_private_ips" {
  value = [for x in module.Instance-Val.private_ip : x]
}
output "ALB_target_ids" {
  value = [for x in module.Instance-Val.instance-id : x]
}
*/

/*
output "vpc_public_subnet_cidr" {
  value = [for x in module.pre-prod-VPC.public_subnet_cidr : x]
}

output "vpc_private_subnet_cidr" {
  value = [for x in module.pre-prod-VPC.private_subnet_cidr : x]
}

output "cluster02_server_public_ip" {
  value = [for x in module.pre-prod-EC2-02.public_ip : x]
}

output "nlb02_dns_endpoint" {
  value = module.pre-prod-NLB-02.nlb-02_dns_endpoint
}

output "nlb-02_target_ids" {
  value = module.pre-prod-NLB-02.nlb_target_ids
}

output "NLB_dns_endpoint" {
  value = module.NLB-HANADB-Val.nlb_dns_endpoint
}

output "NLB_target_ids" {
  value = module.NLB-HANADB-Val.nlb_target_ids
}
*/
