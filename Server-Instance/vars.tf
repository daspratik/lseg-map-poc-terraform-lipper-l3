######################
# General Variables #
#####################
//variable "aws_profile" {type = string}
variable "aws_region" {type = string}
variable "role_arn" {type = string}
variable "session_name" {type = string}
##################
# VPC Variables #
#################
variable "vpc_network" {type = string}
variable "vpc_netmask" {type = string}
variable "vpc_name" {type = string}
variable "public_subnet_count" {type = number}
variable "private_subnet_count" {type = number}
variable "environment" {type = string}
/*
##################
# KMS Variables #
#################
variable "user_arn" {type = string}
variable "key_spec" {type = string}
variable "kms_enabled" {type = bool}
variable "kms_rotation" {type = bool}
variable "kms_deletion_window" {type = number}
*/
##########################
# RSA Key Pair Variables #
#########################
variable "rsa-bits" {type = number}
variable "key-pair-name" {type = string}
//variable "key-pair-count" {type = number}
/*
##################
# EC2 Variables #
#################
variable "image-id" {type = string}
variable "is_bastion" {type = bool}
variable "is_bastion_windows" {type = bool}
variable "bastion-instance-type" {type = string}
variable "name" {type = string}
variable "packer_ami" {type = string}
variable "instance-type" {type = string}
variable "environment" {type = string}
variable "application" {type = string}
variable "business-owner" {type = string}
variable "business-area" {type = string}
variable "owner" {type = string}
variable "service" {type = string}
variable "platform" {type = string}
variable "os-version" {type = string}
variable "instance-tenancy" {type = string}
variable "is_cluster" {type = bool}
variable "root_vol_size" {type = number}
variable "root_vol_type" {type = string}
variable "vol01-size" {type = number}
variable "vol02-size" {type = number}
variable "vol-type" {type = string}
variable "USER_DATA" {type = string}
variable "USERNAME" {type = string}
//variable "PUBLIC_KEY" {type = string}
//variable "PRIVATE_KEY" {type = string}
*/
##################
# ASG Variables #
#################
//variable "image-id" {type = string}
variable "instance-type" {type = string}
variable "USER_DATA" {type = string}
variable "root-vol-size" {type = number}
variable "data-vol-size" {type = number}
variable "vol-type" {type = string}
variable "iops" {type = number}
variable "throughput" {type = number}
variable "asg-health-check-type" {type = string}
variable "min-size" {type = number}
variable "desired-size" {type = number}
variable "max-size" {type = number}
#############################
# Security Group Variables #
############################
//variable "bastion_sg_ports" {type = map(list(string))}
variable "instance_sg_ports" {type = map(list(string))}
variable "alb_sg_ports" {type = map(list(string))}
/*
##################
# NLB Variables #
#################
variable "hdb_nlb_name" {type = string}
variable "hdb_nlb_listener_ports" {type = map(any)}
variable "hdb_nlb_tg_name" {type = string}
variable "hdb_nlb_tg_ports" {type = map(any)}
variable "nlb_target_type" {type = bool}
*/
##################
# ALB Variables #
#################
variable "alb_name" {type = string}
variable "alb_tg_name" {type = string}
variable "alb_tg_ports" {type = map(any)}
variable "alb_target_type" {type = bool}
//variable "alb_listener_ports" {type = map(any)}
//variable "alb_listener_ports" {type = list(string)}
##################
# ACM Variables #
#################
variable "domain-name" {type = string}
variable "root-domain-name" {type = string}
variable "subdomain-name" {type = string}

/*
variable "aws_profile" {
  type    = string
  default = "sap-harmony"
}
//variable "vpc-sec-grp-ids" {type = list(string)}
//variable "instance_subnet_id" {type = string}
//variable "vpc-id" {type = string}
//variable "nlb_subnet_ids" {type = list(string)}
variable "kms-key-id" {type = string}
variable "name" {type = string}
variable "ami_id" {type = string}
variable "vol03-size" {type = number}
variable "vol04-size" {type = number}
variable "vol05-size" {type = number}
variable "vol06-size" {type = number}
variable "vol07-size" {type = number}
variable "vol08-size" {type = number}
variable "vol09-size" {type = number}
variable "vol10-size" {type = number}
*/
