data "aws_caller_identity" "current_account" {}

data "aws_ami" "packer_ami"{
  most_recent = "true"
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  owners = [data.aws_caller_identity.current_account.account_id]
}

# Generate a private TLS key using RSA encryption with a length of 4096 bits
resource "tls_private_key" "ssh_private_key" {
  //count   = var.key-pair-count
  algorithm = "RSA"
  rsa_bits  = var.rsa-bits
}

# Leverage the TLS private key to generate a SSH public/private key pair
resource "aws_key_pair" "ssh_key_pair" {
  depends_on = [tls_private_key.ssh_private_key]
  //count    = var.key-pair-count
  key_name   = var.key-pair-name
  public_key = tls_private_key.ssh_private_key.public_key_openssh

  # when creating, save the private key to the local Terraform directory
  provisioner "local-exec" {
    when = create
    //command = "sudo echo '${tls_private_key.ssh_private_key.private_key_pem}' > ${var.key-pair-name}.pem"
    command = <<-EOT
      echo '${tls_private_key.ssh_private_key.private_key_pem}' > '${var.key-pair-name}'.pem
      chmod 400 '${var.key-pair-name}'.pem
    EOT
  }

  # When this resources is destroyed, delete the associated key from the file system
  provisioner "local-exec" {
    when    = destroy
    command = "rm -rf ${self.id}.pem"
  }
}

# Apply correct file/directory level permission
resource "local_file" "pem-file" {
  depends_on            = [tls_private_key.ssh_private_key]
  //count               = var.key-pair-count
  content               = tls_private_key.ssh_private_key.private_key_pem
  filename              = "${var.key-pair-name}.pem"
  directory_permission  = "0777"
  //file_permission     = "0400"
}
/*
data "aws_instances" "alb_targets" {
  instance_state_names = ["running"]
  instance_tags = {
    Terraform = "true"
  }
}
*/
# Create VPC
module "L3-Dev-VPC" {
  //source = "git::https://github.com/github-daspratik/vpc-module.git"
  source               = "../Modules/VPC"
  vpc_name             = var.vpc_name
  vpc_network          = var.vpc_network
  vpc_netmask          = var.vpc_netmask
  environment          = var.environment
  private_subnet_count = var.private_subnet_count
  public_subnet_count  = var.public_subnet_count
}

# Create Security Group
module "L3-Dev-SG" {
  //source = "git::https://github.com/github-daspratik/security-group-module.git"
  source              = "../Modules/SECURITY-GROUP"
  vpc-id              = module.L3-Dev-VPC.vpc-id
  //bastion_sg_ports    = var.bastion_sg_ports
  instance_sg_ports   = var.instance_sg_ports
  alb_sg_ports        = var.alb_sg_ports
}
/*
# Create Server(s) in VAL
module "Instance-Val" {
  //source = "git::https://github.com/github-daspratik/ec2-instance-module.git"
  source                  = "../Modules/EC2"
  is_bastion              = var.is_bastion
  is_bastion_windows      = var.is_bastion_windows
  bastion_instance_type   = var.bastion-instance-type
  packer-ami              = var.packer_ami
  name                    = var.name
  environment             = var.environment
  application             = var.application
  business-area           = var.business-area
  business-owner          = var.business-owner
  owner                   = var.owner
  service                 = var.service
  platform                = var.platform
  os-version              = var.os-version
  instance-tenancy        = var.instance-tenancy
  instance-type           = var.instance-type
  instance_count          = var.public_subnet_count + var.private_subnet_count
  is_cluster              = var.is_cluster
  vpc_id                  = module.VPC-Val.vpc-id
  instance_subnet_ids     = module.VPC-Val.public_subnet_ids
  instance-sec-grp-ids    = [module.SG-Val.instance-sg-id]
  bastion-sec-grp-ids     = [module.SG-Val.bastion-sg-id]
  USER_DATA               = var.USER_DATA
  USERNAME                = var.USERNAME
  root_vol_size           = var.root_vol_size
  root_vol_type           = var.root_vol_type
  PUBLIC_KEY              = var.key-pair-name
  PRIVATE_KEY             = "${var.key-pair-name}.pem"
  //PUBLIC_KEY        = module.RSA-SSH.ssh-key-public
  //PRIVATE_KEY       = module.RSA-SSH.ssh-key-private
  //vol-type          = var.vol-type
  //vol01-size        = var.vol01-size
  //vol02-size        = var.vol02-size
  //kms-key-ids       = module.KMS-Val.kms_key_arn
}

module "EBS-VOL" {
  //source = "git::https://github.com/github-daspratik/ebs-volume-module.git"
  source = "../Modules/EBS"
  instance-count = var.public_subnet_count + var.private_subnet_count
  instance-ids = module.Instance-Val.instance-id
  vol-type = var.vol-type
  vol01-size = var.vol01-size
  vol02-size = var.vol02-size
  //kms-key-ids = module.KMS-Val.kms_key_arn
}
*/
# Create an Application Load Balancer
module "L3-Dev-ALB" {
  source              = "../Modules/ALB"
  vpc-id              = module.L3-Dev-VPC.vpc-id
  alb_subnet_ids      = module.L3-Dev-VPC.public_subnet_ids
  alb_sec_groups      = [module.L3-Dev-SG.alb-sg-id]
  alb_name            = var.alb_name
  alb_tg_name         = var.alb_tg_name
  alb_tg_ports        = var.alb_tg_ports
  alb_target_type     = var.alb_target_type
  alb_sg_ports        = var.alb_sg_ports
  certificate_arn     = module.L3-Dev-ACM.acm-certificate-arn
  asg-name            = module.L3-Dev-ASG.autoscaling-group-name
  //alb_targets         = data.aws_instances.alb_targets.ids
  //alb_targets         = module.Instance-Val.instance-id
  //alb_listener_ports  = var.alb_listener_ports
}
module "L3-Dev-ACM" {
  source = "../Modules/ACM"
  domain_name = var.domain-name
  root_domain_name = var.root-domain-name
  subdomain_name = var.subdomain-name
  alias_name = module.L3-Dev-ALB.alb_dns_endpoint
  alias_zone_id = module.L3-Dev-ALB.alb_zone_id
}
module "L3-Dev-ASG" {
  source = "../Modules/ASG"
  //image-id = var.image-id
  image-id = data.aws_ami.packer_ami.id
  PUBLIC_KEY = var.key-pair-name
  USER_DATA = var.USER_DATA
  //alb-id = module.ALB-HANADB.alb_id
  alb-target-group-arns = module.L3-Dev-ALB.alb_tg_arn
  asg-subnets = module.L3-Dev-VPC.public_subnet_ids
  root_vol_size = var.root-vol-size
  data_vol_size = var.data-vol-size
  vol_type = var.vol-type
  iops = var.iops
  throughput = var.throughput
  asg-health-check-type = var.asg-health-check-type
  min-size = var.min-size
  desired-size = var.desired-size
  max-size = var.max-size
  instance-type = var.instance-type
  instance-sec-grp-ids = module.L3-Dev-SG.instance-sg-id
}
/*
# Create Network-Load-Balancer in VAL
module "NLB-HANADB-Val" {
  //source = "git::https://github.com/github-daspratik/nlb-module.git"
  source                 = "../Modules/NLB"
  vpc-id                 = module.VPC-Val.vpc-id
  nlb_target_type        = var.nlb_target_type
  nlb_subnet_ids         = module.VPC-Val.public_subnet_ids
  hdb_nlb_name           = var.hdb_nlb_name
  hdb_nlb_tg_name        = var.hdb_nlb_tg_name
  hdb_nlb_listener_ports = var.hdb_nlb_listener_ports
  hdb_nlb_tg_ports       = var.hdb_nlb_tg_ports
}
#Create KMS keys
module "KMS-Val" {
  //source = "git::https://github.com/github-daspratik/kms-module.git"
  source              = "../Modules/KMS"
  name                = var.name
  instance_count      = var.public_subnet_count + var.private_subnet_count
  is_cluster          = var.is_cluster
  key_spec            = var.key_spec
  kms_deletion_window = var.kms_deletion_window
  kms_enabled         = var.kms_enabled
  kms_rotation        = var.kms_rotation
  role_arn            = var.role_arn
  user_arn            = var.user_arn
}
#Create RSA SSH Keys
module "RSA-SSH" {
  //source = "git::https://github.com/github-daspratik/ssh-key-pair-module.git"
  source         = "../Modules/SSH-KEY-PAIR"
  key-pair-count = var.public_subnet_count + var.private_subnet_count
  key-pair-name  = var.key-pair-name
  rsa_bits       = var.rsa-bits
}
*/
