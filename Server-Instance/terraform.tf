# Terraform Block
terraform {
  required_version = ">=1.1.7"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.30.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">=3.1.0"
    }
  }

  backend "s3" {
    //bucket       = "terraform-tfstate-s3-storage"
    bucket       = "lseg-map-lipper-l3-dev"
    key          = "l3-dev-tfstate"
    //role_arn     = "arn:aws:iam::163898482461:role/terraform-role"
    //role_arn     = "arn:aws:iam::649711108397:role/IaC-Role"
    role_arn     = "arn:aws:iam::163898482461:role/MyCloud-Role"
    session_name = "tf_sts_session"
    encrypt      = "true"
    region       = "us-west-2"
    //profile = "sap-val"
  }
}
