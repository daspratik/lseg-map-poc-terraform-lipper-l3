# Provider Block
provider "aws" {
  //profile = var.aws_profile
  region = var.aws_region
  # Declaring AssumeRole
  assume_role {
    # The Role ARN is the Amazon Resource Name of the Cross Account IAM Role for this Account's Terraform CLI to Assume
    role_arn = var.role_arn
    # Declaring a STS session name
    session_name = var.session_name
  }
}